# Node modules SVG loader
Plugin for fetching specified SVGs from npm or any relative destination (e.g. FontAwesome, subfolder) to project's source folder, where you can build your own subset svg sprite or anything you desire. You can then have every icon within reach, while keeping project sprite as minimal as possible.

## Installation and configuration
Install with
```
npm i -D @superkoders/node-modules-svg-loader
```

And then make gulp task like this or similar, which integrate with your workflow.

```js
const svgLoader = require('@superkoders/node-modules-svg-loader');

module.exports = function svgLoad(done) {
	svgLoader({
		iconSource: './fontawesome-pro-5.11.2-web/svgs',
		projectIconFolder: './src/img/bg/icons-svg/',
		iconList: './src/img/icon-list.json',
	}, done);
	done();
};
```

In the task specify:
1. root folder of your source icons in `iconSource`.
2. path to folder, where you keep icons you use in the project.
3. path to the JSON where you specify which icons you want to load.

## Usage
### icon-list.json
For each icon specify relative path from `iconSource` and new name. You can also specify path, if you need to create new structure. Extension `.svg` is going to be added automatically.
```json
{
	"icons": [
		{
			"finalName": "technlogy-brain",
			"src": "02-Technology/01-Artificial-Intelligence/brain"
		},
		{
			"finalName": "technlogy/ai/brain",
			"src": "02-Technology/01-Artificial-Intelligence/brain"
		},
	]
}
```

## Author
[![N|Superkoders](http://logo.superkoderi.cz/superkoders.svg)](https://www.superkoders.com)

## License
This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
