const { src, dest } = require('gulp');
const fs = require('fs');
const path = require('path');
const chalk = require('chalk');
const rename = require('gulp-rename');
const through2 = require('through2');
const notify = require('gulp-notify');
const plumber = require('gulp-plumber');

module.exports = (packageOptions) => function loadSvg(done) {
	const onError = function (error) {
		notify.onError({
				title: 'SVG loader error.',
				message: '<%= error.message %>',
				sound: 'Beep',
			})(error);
			done();
		};

	return src(path.resolve('.', packageOptions.iconList))
	.pipe(plumber({
		errorHandler: onError
	}))
		.pipe(through2.obj(function(file, enc, cb) {

			const {name, ext, dir} = path.parse(file.path);
			const iconListFile = JSON.parse(file.contents.toString('utf8'));

			iconListFile.icons.forEach(icon => {
				let currentIcon = path.resolve('.', packageOptions.iconSource, icon.src + ".svg" );

				try {
					currentIcon = fs.readFileSync(currentIcon, { encoding: 'utf-8'});
				} catch(err) {
					console.warn(chalk.red.bold('Icon ' + icon.src + "/" + ' was not found'), 'in ' + packageOptions.iconSource + '. Skipping.\nMake sure you specified valid icon in ' + packageOptions.iconList);
				}

				const newFile = file.clone();
				newFile.contents = Buffer.from(currentIcon);
				newFile.path = path.join(dir, `${icon.finalName}.svg`);
				this.push(newFile);
			});

			cb();
		}))
		.pipe(dest(path.resolve('.', packageOptions.projectIconFolder)))
};
