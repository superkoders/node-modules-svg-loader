const { series } = require('gulp');
const deepmerge = require('deepmerge');
const loadSvg = require('./tasks/loadSvg.js');

const defaultOptions = {
	iconSource: 'node_modules/@fortawesome/@fontawesome-free/svgs/',
	projectIconFolder: './src/img/bg/icons-svg/',
	iconList: './src/img/bg/icon-list.json'
};

module.exports = function defaultTask(userOptions, done) {
	const options = deepmerge(defaultOptions, userOptions);
	const loadSvgTask = loadSvg(options);

	return series(loadSvgTask)(done);
};
